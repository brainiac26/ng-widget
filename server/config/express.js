require('dotenv').config();

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const compress = require('compression');
const cors = require('cors');

module.exports = function() {
  const app = express();

  app.use(cors());

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));

  if (process.env.NODE_ENV === 'production') {
    app.use(compress());
  } else {
    app.use(morgan('dev'));
  }

  app.use(express.static('./dist'));

  require('../app/router.js')(app);

  app.get('/*', function(req, res) {
    res.sendFile(path.join(__dirname, '../../dist/index.html'));
  });

  return app;
};




