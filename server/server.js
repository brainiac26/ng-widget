process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const express = require('./config/express.js');
const ddb = require('./config/ddb.js');

const port = process.env.PORT || 3000;

const app = express();

app.listen(port, function() {
  console.log('Server listening on port ' + port);
});
