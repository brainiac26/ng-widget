const dynamo = require('dynamodb');
const Joi = require('joi');

const Ticket = dynamo.define('Ticket', {
  hashKey: 'id',

  timestamps : false,

  schema : {
    id: dynamo.types.uuid(),

    createdUserId: Joi.string().optional().allow(''),

    title: Joi.string().optional().allow(''),
    type: Joi.string().optional().allow(''),
    status: Joi.string().optional().allow(''),
    priority: Joi.string().optional().allow(''),
    description: Joi.string().optional().allow(''),

    createdTimestamp: Joi.number().integer(),
    modifiedTimestamp: Joi.number().integer()
  },
  tableName: 'tickets'
});

module.exports = Ticket;
