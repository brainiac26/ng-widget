const dynamo = require('dynamodb');
const Joi = require('joi');

const User = dynamo.define('User', {
  hashKey: 'id',

  timestamps : false,

  schema : {
    id: dynamo.types.uuid(),

    association: Joi.string().optional().allow(''),
    email: Joi.string().email().allow(''),
    name: Joi.string().optional().allow(''),
    username: Joi.string().optional().allow('')
  },
  tableName: 'users'
});

module.exports = User;
