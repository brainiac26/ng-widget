const dynamo = require('dynamodb');
const Joi = require('joi');

const Note = dynamo.define('Note', {
  hashKey: 'id',

  timestamps : false,

  schema : {
    id: dynamo.types.uuid(),
    userId: Joi.string().optional().allow(''),
    title: Joi.string().optional().allow(''),
    type: Joi.string(),
    body: Joi.string().optional().allow(''),
    drawingUrl: Joi.string().optional().allow(''),
    recordingUrl: Joi.string().optional().allow(''),
    checklist: Joi.array().items({ text: Joi.string(), done: Joi.boolean() }),

    createdTimestamp: Joi.number().integer(),
    modifiedTimestamp: Joi.number().integer()
  },
  tableName: 'notes'
});

module.exports = Note;
