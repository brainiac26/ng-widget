const dynamo = require('dynamodb');
const Joi = require('joi');

const Task = dynamo.define('Task', {
  hashKey: 'id',

  timestamps : false,

  schema : {
    id: dynamo.types.uuid(),

    createdUserId: Joi.string().optional().allow(''),
    assignedUserId: Joi.string().optional().allow(''),

    title: Joi.string().optional().allow(''),
    description: Joi.string().optional().allow(''),
    status: Joi.string().optional().allow(''),
    dueDate: Joi.number().integer(),
    priority: Joi.string().optional().allow(''),

    createdTimestamp: Joi.number().integer(),
    modifiedTimestamp: Joi.number().integer()
  },
  tableName: 'tasks'
});

module.exports = Task;
