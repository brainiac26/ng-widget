const dynamo = require('dynamodb');
const Joi = require('joi');

const Monitoring = dynamo.define('Monitoring', {
  hashKey: 'id',

  timestamps : false,

  schema : {
    id: dynamo.types.uuid(),

    userId: Joi.string().optional().allow(''),
    entityType: Joi.string().optional().allow(''),
    entitySubType: Joi.string().optional().allow(''),
    entityId: Joi.string().optional().allow(''),
    eventType: Joi.string().optional().allow(''),
    events: Joi.array().items({
      fieldName: Joi.string().optional().allow(''),
      oldValue: Joi.string().optional().allow(''),
      newValue: Joi.string().optional().allow('')
    }),
    timestamp: Joi.number().integer()
  },
  tableName: 'monitoring'
});

module.exports = Monitoring;
