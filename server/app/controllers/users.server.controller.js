const User = require('../models/user.model');

exports.list = function(req, res, next) {
  User
    .scan()
    .loadAll()
    .exec(function(err, response) {
      if (err) return next(err);
      return res.send(response.Items);
    });
};
