const Note = require('../models/note.model');

exports.list = function(req, res, next) {
  Note
    .scan()
    .loadAll()
    .exec(function(err, response) {
      if (err) return next(err);
      return res.send(response.Items);
    });
};

exports.create = function(req, res, next) {
  const note = new Note(Object.assign(req.body, {
    createdTimestamp: new Date().getTime(),
    modifiedTimestamp: new Date().getTime()
  }));

  note.save(function(err) {
    if (err) return next(err);
    return res.send(note);
  })
};

exports.update = function(req, res, next) {
  const id = req.params['id'];
  const data = Object.assign({ id: id }, { modifiedTimestamp: new Date().getTime() }, req.body);
  Note.update(data, function (err, note) {
    if (err) return next(err);
    return res.send(note);
  });
};

exports.delete = function(req, res, next) {
  const id = req.params['id'];
  Note.destroy(id, function(err) {
    if (err) return next(err);
    return res.send({});
  })
};
