const s3 = require('../../config/aws').s3;

exports.upload = function(req, res, next) {
  const file = req.files[0];
  const params = {
    Bucket: 'ng-widget-data',
    Key: file.fieldname,
    Body: file.buffer,
    ContentType: file.mimetype,
    ACL: 'public-read',
    CacheControl: 'no-cache'
  };
  s3.upload(params, function(err, data) {
    if (err) return next(err);
    return res.send({url: data.Location});
  });
};
