const Monitoring = require('../models/monitoring.model');

exports.list = function(req, res, next) {
  Monitoring
    .scan()
    .loadAll()
    .exec(function(err, response) {
      if (err) return next(err);
      return res.send(response.Items);
    });
};

exports.create = function(req, res, next) {

  const monitoring = new Monitoring(req.body);
  monitoring.timestamp = new Date().getTime();

  monitoring.save(function(err) {
    if (err) return next(err);
    return res.send(monitoring);
  })
};

