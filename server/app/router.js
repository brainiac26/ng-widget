const multer = require('multer');
const notesController = require('./controllers/notes.server.controller');
const remindersController = require('./controllers/reminders.server.controller');
const tasksController = require('./controllers/tasks.server.controller');
const ticketsController = require('./controllers/tickets.server.controller');
const usersController = require('./controllers/users.server.controller');
const filesController = require('./controllers/files.server.controller');
const monitoringController = require('./controllers/monitoring.server.controller');
const errorHandler = require('./utils/errorHandler');
const upload = multer();

module.exports = function(app) {

  // notes

  app.get('/api/notes', notesController.list);

  app.post('/api/notes', notesController.create);

  app.put('/api/notes/:id', notesController.update);

  app.delete('/api/notes/:id', notesController.delete);

  // reminders

  app.post('/api/notes/:noteId/reminders', remindersController.create);

  // tasks

  app.get('/api/tasks', tasksController.list);

  app.post('/api/tasks', tasksController.create);

  app.put('/api/tasks/:id', tasksController.update);

  app.delete('/api/tasks/:id', tasksController.delete);

  // tickets

  app.get('/api/tickets', ticketsController.list);

  app.post('/api/tickets', ticketsController.create);

  app.put('/api/tickets/:id', ticketsController.update);

  app.delete('/api/tickets/:id', ticketsController.delete);

  // users

  app.get('/api/users', usersController.list);

  // files

  app.post('/api/files/upload', upload.any(), filesController.upload);

  //monitoring

  app.get('/api/monitoring', monitoringController.list);

  app.post('/api/monitoring', monitoringController.create);

  // error handler

  app.use(errorHandler);

};
