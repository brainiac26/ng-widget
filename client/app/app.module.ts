import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

import { createCustomElement } from '@angular/elements';
import { APP_BASE_HREF } from '@angular/common';
import { WidgetComponent } from './widget/widget.component';
import { NotesComponent } from './widget/notes/notes.component';
import { MenuComponent } from './widget/menu/menu.component';
import { EditNoteComponent } from './widget/notes/edit-note/edit-note.component';
import { SetReminderComponent } from './widget/notes/set-reminder/set-reminder.component';
import {EditNoteGuard} from './widget/guards/edit-note.guard';
import {NotesModule} from './widget/notes/notes.module';
import {MenuModule} from './widget/menu/menu.module';
import {TasksModule} from './widget/tasks/tasks.module';
import {TasksComponent} from './widget/tasks/tasks.component';
import {TicketsComponent} from './widget/tickets/tickets.component';
import {TicketsModule} from './widget/tickets/tickets.module';
import {NewItemComponent} from './widget/shared/new-item/new-item.component';
import {EditItemComponent} from './widget/shared/edit-item/edit-item.component';
import {EditItemGuard} from './widget/guards/edit-item.guard';



const routes = [
  { path: '', component: MenuComponent, outlet: 'outlet' },

  { path: 'tasks', component: TasksComponent, outlet: 'outlet' },

  { path: 'tickets', component: TicketsComponent, outlet: 'outlet' },

  { path: 'new-item', component: NewItemComponent, outlet: 'outlet' },
  { path: 'edit-item', component: EditItemComponent, outlet: 'outlet', canDeactivate: [EditItemGuard] },

  { path: 'notes', component: NotesComponent, outlet: 'outlet' },
  { path: 'edit-note', component: EditNoteComponent, outlet: 'outlet', canDeactivate: [EditNoteGuard] },

  { path: 'reminder', component: SetReminderComponent, outlet: 'outlet' },
];

@NgModule({
  declarations: [
    WidgetComponent
  ],
  imports: [
    BrowserModule,
    RouterTestingModule.withRoutes(routes),
    MenuModule,
    NotesModule,
    TasksModule,
    TicketsModule
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  entryComponents: [WidgetComponent]
})
export class AppModule {
  constructor(private injector: Injector) { }
  ngDoBootstrap() {
    const el = createCustomElement(WidgetComponent, {
      injector: this.injector
    });
    customElements.define('ng-widget', el);
  }
}
