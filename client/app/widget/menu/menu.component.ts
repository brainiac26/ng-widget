import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  items = [
    {
      name: 'notes',
      caption: 'Notes',
      icon: 'icon-pen',
      canAdd: true,
      route: 'notes'
    },
    {
      name: 'tasks',
      caption: 'Tasks',
      icon: 'icon-check',
      canAdd: true,
      route: 'tasks'
    },
    {
      name: 'support',
      caption: 'Support',
      icon: 'icon-tools',
      canAdd: false,
      route: 'tickets'
    },
    {
      name: 'share',
      caption: 'Share screen',
      icon: 'icon-share-screen',
      canAdd: false,
      route: ''
    },
    {
      name: 'chat',
      caption: 'Chat',
      icon: 'icon-comments-alt',
      canAdd: false,
      route: ''
    },
    {
      name: 'settings',
      caption: 'Settings',
      icon: 'icon-cog',
      canAdd: false,
      route: ''
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
