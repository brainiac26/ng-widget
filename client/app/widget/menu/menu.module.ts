import { NgModule } from '@angular/core';
import {CoreModule} from '../core/core.module';
import {MenuComponent} from './menu.component';
import {MenuItemComponent} from './menu-item/menu-item.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [MenuComponent, MenuItemComponent],
  imports: [
    CoreModule,
    SharedModule
  ],
  exports: [MenuComponent]
})
export class MenuModule { }
