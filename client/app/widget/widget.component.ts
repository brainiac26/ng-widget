import {Component, Input, OnChanges, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';

import {NavigationService} from './services/navigation.service';
import {NotesService} from './services/notes.service';
import {UserService} from './services/user.service';
import {TasksService} from './services/tasks.service';
import {UsersService} from './services/users.service';
import {TicketsService} from './services/tickets.service';

@Component({
  selector: 'ng-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class WidgetComponent implements OnInit, OnChanges {

  @Input() userId: string;
  @Input() username: string;
  @Input() fullname: string;
  @Input() email: string;
  @Input() groupid: string;

  constructor( private router: Router,
               private notesService: NotesService,
               private tasksService: TasksService,
               private ticketsService: TicketsService,
               private navigationService: NavigationService,
               private usersService: UsersService,
               private userService: UserService) {}

  isPopupShown = false;

  selectedItem: any;

  ngOnInit(): void {
    this.router.initialNavigation();

    this.navigationService.close.subscribe(() => {
      this.router.navigate(['/']);
      this.isPopupShown = false;
    });

    this.notesService.list().subscribe();
    this.tasksService.list().subscribe();
    this.usersService.list().subscribe();
    this.ticketsService.list().subscribe();
  }

  ngOnChanges(): void {
    console.log(this.userId);
    console.log(this.username);
    console.log(this.fullname);
    console.log(this.email);
    console.log(this.groupid);

    this.userService.saveData(this.userId, this.username, this.fullname, this.email, this.groupid);

    this.notesService.list().subscribe();
    this.tasksService.list().subscribe();
    this.usersService.list().subscribe();
    this.ticketsService.list().subscribe();
  }

  goBack() {
    this.selectedItem = null;
  }

  toggle() {
    this.isPopupShown = !this.isPopupShown;
    if (!this.isPopupShown) {
      this.selectedItem = null;
    }
  }

  selectItem(item) {
    if (item.disabled) return;
    this.selectedItem = item;
  }

  isSelectedItem(itemName) {
    return this.selectedItem && this.selectedItem.name === itemName;
  }

}
