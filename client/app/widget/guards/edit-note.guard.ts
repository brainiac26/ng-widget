import {CanDeactivate} from "@angular/router";
import {Observable} from "rxjs";
import {EditNoteComponent} from '../notes/edit-note/edit-note.component';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EditNoteGuard implements CanDeactivate<EditNoteComponent>{

  canDeactivate(component: EditNoteComponent) : Observable<boolean> | boolean{

    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
