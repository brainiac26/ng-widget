import {CanDeactivate} from "@angular/router";
import {Observable} from "rxjs";
import {Injectable} from '@angular/core';
import {EditItemComponent} from '../shared/edit-item/edit-item.component';

@Injectable({
  providedIn: 'root'
})
export class EditItemGuard implements CanDeactivate<EditItemComponent>{

  canDeactivate(component: EditItemComponent) : Observable<boolean> | boolean{

    return component.canDeactivate ? component.canDeactivate() : true;
  }
}
