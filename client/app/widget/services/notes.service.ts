import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {switchMap, tap} from 'rxjs/internal/operators';
import {BehaviorSubject, Observable} from 'rxjs';
import {MonitoringService} from './monitoring.service';

@Injectable({
  providedIn: 'root'
})
export class NotesService {

  notes: any[];

  note: any;

  sortOption: any;

  updateNotesManager: BehaviorSubject<any[]>;

  updateNotes: Observable<any[]>;

  constructor(private http: HttpClient, private monitoringService: MonitoringService) {
    this.updateNotesManager = new BehaviorSubject<any>(null);
    this.updateNotes = this.updateNotesManager.asObservable();
  }

  list() {
    return this.http.get('/api/notes')
      .pipe(tap((res: any[]) => {
        this.notes = res;
        this.updateNotesManager.next(this.notes);

      }))
  }

  create(data, changes) {
    return this.http.post('/api/notes', data)
      .pipe(tap((res: any) => {
        this.notes = [res, ...this.notes];
        this.updateNotesManager.next(this.notes);

        const monitoringObject = this.monitoringService.getMonitoringForNote(res, changes, false);

        this.monitoringService.create(monitoringObject).subscribe();
      }))
  }

  createReminder(data, noteId) {
    return this.http.post(`/api/notes/${noteId}/reminders`, data);
  }

  update(id, data, changes) {
    return this.http.put(`/api/notes/${id}`, data)
      .pipe(
        tap((res: any) => {
            this.notes = this.notes.map(note => {
              if (note.id === res.id) {
                note = res;
              }
              return note;
            });
            this.updateNotesManager.next(this.notes);

            const monitoringObject = this.monitoringService.getMonitoringForNote(res, changes, true);

            this.monitoringService.create(monitoringObject).subscribe();
          })
      )
  }

  delete(id) {
    return this.http.delete(`/api/notes/${id}`)
      .pipe(tap((res: any) => {
        this.notes = this.notes.filter(note => note.id !== id);
        this.updateNotesManager.next(this.notes);
      }))
  }

  getSavedNote(id) {
    return this.notes.find(note => note.id === id);
  }

  getSortOption() {
    return this.sortOption;
  }

  setSortOption(option) {
    this.sortOption = option;
  }
}
