import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { v4 as uuidv4 } from 'uuid';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FilesService {

  constructor(private http: HttpClient) { }

  upload(file: File, previousLink?: string) {
    let filename;
    if (previousLink) {
      const linkArr = previousLink.split('/');
      filename = linkArr[linkArr.length - 1];
    } else {
      filename = uuidv4();
    }

    const formData = new FormData();
    formData.append(filename, file);
    return this.http.post('/api/files/upload', formData);
  }
}
