import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';
import {MonitoringService} from './monitoring.service';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  tickets: any[];

  ticket: any;

  sortOption: any;

  updateItemsManager: BehaviorSubject<any[]>;

  updateItems: Observable<any[]>;

  constructor(private http: HttpClient, private monitoringService: MonitoringService) {
    this.updateItemsManager = new BehaviorSubject<any>(null);
    this.updateItems = this.updateItemsManager.asObservable();
  }

  list() {
    return this.http.get('/api/tickets')
      .pipe(tap((res: any[]) => {
        this.tickets = res;
        this.updateItemsManager.next(this.tickets);
      }))
  }

  create(data, changes) {
    return this.http.post('/api/tickets', data)
      .pipe(tap((res: any) => {
        this.tickets = [res, ...this.tickets];
        this.updateItemsManager.next(this.tickets);

        const monitoringObject = this.monitoringService.getMonitoringForItem(res, 'ticket', changes, false);

        this.monitoringService.create(monitoringObject).subscribe();
      }))
  }

  update(id, data, changes) {
    return this.http.put(`/api/tickets/${id}`, data)
      .pipe(tap((res: any) => {
        this.tickets = this.tickets.map(ticket => {
          if (ticket.id === res.id) {
            ticket = res;
          }
          return ticket;
        });
        this.updateItemsManager.next(this.tickets);

        const monitoringObject = this.monitoringService.getMonitoringForItem(res, 'ticket', changes, true);

        this.monitoringService.create(monitoringObject).subscribe();
      }))
  }

  delete(id) {
    return this.http.delete(`/api/tickets/${id}`)
      .pipe(tap((res: any) => {
        this.tickets = this.tickets.filter(ticket => ticket.id !== id);
        this.updateItemsManager.next(this.tickets);
      }))
  }

  getSavedItem(id) {
    return this.tickets.find(ticket => ticket.id === id);
  }

  getSortOption() {
    return this.sortOption;
  }

  setSortOption(option) {
    this.sortOption = option;
  }

}
