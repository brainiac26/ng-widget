import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavigationService {

  private closeManager: BehaviorSubject<void> = new BehaviorSubject(null);
  close: Observable<void>;

  constructor() {
    this.close = this.closeManager.asObservable();
  }

  doClose() {
    this.closeManager.next(null);
  }
}
