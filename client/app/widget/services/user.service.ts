import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userId: string;
  username: string;
  fullname: string;
  email: string;
  groupid: string;

  constructor() { }

  // hardcode userId for now
  saveData(userId: string = '12345', username: string, fullname: string, email: string, groupid: string) {
    this.userId = userId;
    this.username = username;
    this.fullname = fullname;
    this.email = email;
    this.groupid = groupid;
  }
}
