import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class MonitoringService {

  constructor(private http: HttpClient, private userService: UserService) { }

  getMonitoringForNote(note: any, changes: any[], isEdit: boolean) {
    return this.getMonitoring(note.id, 'note', note.type, isEdit, changes);
  }

  getMonitoringForItem(item: any, type, changes: any[], isEdit: boolean) {
    return this.getMonitoring(item.id, type, '', isEdit, changes);
  }

  getMonitoring(id, type, subtype, isEdit, changes) {
    return {
      userId: this.userService.userId,
      entityType: type,
      entitySubType: subtype,
      entityId: id, // fill in after save
      eventType: isEdit ? 'UpdateField' : 'NewEntity',
      events: changes
    };
  }

  list() {
    return this.http.get('/api/monitoring');
  }

  create(data) {
    return this.http.post('/api/monitoring', data);
  }
}
