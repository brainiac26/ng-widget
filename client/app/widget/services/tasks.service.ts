import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';
import {MonitoringService} from './monitoring.service';

@Injectable({
  providedIn: 'root'
})
export class TasksService {

  tasks: any[];

  task: any;

  sortOption: any;

  updateItemsManager: BehaviorSubject<any[]>;

  updateItems: Observable<any[]>;

  constructor(private http: HttpClient, private monitoringService: MonitoringService) {
    this.updateItemsManager = new BehaviorSubject<any>(null);
    this.updateItems = this.updateItemsManager.asObservable();
  }

  list() {
    return this.http.get('/api/tasks')
      .pipe(tap((res: any[]) => {
        this.tasks = res;
        this.updateItemsManager.next(this.tasks);
      }))
  }

  create(data, changes) {
    return this.http.post('/api/tasks', data)
      .pipe(tap((res: any) => {
        this.tasks = [res, ...this.tasks];
        this.updateItemsManager.next(this.tasks);

        const monitoringObject = this.monitoringService.getMonitoringForItem(res, 'task', changes, false);

        this.monitoringService.create(monitoringObject).subscribe();
      }))
  }

  update(id, data, changes) {
    return this.http.put(`/api/tasks/${id}`, data)
      .pipe(tap((res: any) => {
        this.tasks = this.tasks.map(task => {
          if (task.id === res.id) {
            task = res;
          }
          return task;
        });
        this.updateItemsManager.next(this.tasks);

        const monitoringObject = this.monitoringService.getMonitoringForItem(res, 'task', changes, true);

        this.monitoringService.create(monitoringObject).subscribe();
      }))
  }

  delete(id) {
    return this.http.delete(`/api/tasks/${id}`)
      .pipe(tap((res: any) => {
        this.tasks = this.tasks.filter(task => task.id !== id);
        this.updateItemsManager.next(this.tasks);
      }))
  }

  getSavedItem(id) {
    return this.tasks.find(task => task.id === id);
  }

  getSortOption() {
    return this.sortOption;
  }

  setSortOption(option) {
    this.sortOption = option;
  }
}
