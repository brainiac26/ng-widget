import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {tap} from 'rxjs/internal/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: any[];

  constructor(private http: HttpClient) { }

  list() {
    return this.http.get('/api/users')
      .pipe(tap((res: any[]) => {
        this.users = res;
      }))
  }

  findUser(id: string) {
    return this.users.find(user => user.id === id);
  }
}
