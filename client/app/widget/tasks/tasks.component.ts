import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  sortOptions = [
    { caption: 'Title', field: 'title', caseInsensitive: true },
    { caption: 'Created date (newest first)', reverse: true, field: 'createdTimestamp' },
    { caption: 'Created date (oldest first)', field: 'createdTimestamp' },
    { caption: 'Due date (newest first)', reverse: true, field: 'dueDate' },
    { caption: 'Due date (oldest first)', field: 'dueDate' },
    { caption: 'Priority (high first)', comparator: (a) => a.priority === 'high' ? -1 : 1 },
    { caption: 'Priority (normal first)', comparator: (a) => a.priority === 'normal' ? -1 : 1},
    { caption: 'Priority (low first)', comparator: (a) => a.priority === 'low' ? -1 : 1}
  ];

  constructor() {}

  ngOnInit(): void {}

}
