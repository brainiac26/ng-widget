import { NgModule } from '@angular/core';
import {CoreModule} from '../core/core.module';
import {SharedModule} from '../shared/shared.module';
import { TasksComponent } from './tasks.component';



@NgModule({
  declarations: [
    TasksComponent
  ],
  imports: [
    CoreModule,
    SharedModule
  ]
})
export class TasksModule { }
