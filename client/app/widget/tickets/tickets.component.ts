import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {

  sortOptions = [
    { caption: 'Title', field: 'title', caseInsensitive: true },
    { caption: 'Created date (newest first)', reverse: true, field: 'createdTimestamp' },
    { caption: 'Created date (oldest first)', field: 'createdTimestamp' },
    { caption: 'Priority (high first)', comparator: (a) => a.priority === 'high' ? -1 : 1 },
    { caption: 'Priority (normal first)', comparator: (a) => a.priority === 'normal' ? -1 : 1},
    { caption: 'Priority (low first)', comparator: (a) => a.priority === 'low' ? -1 : 1}
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
