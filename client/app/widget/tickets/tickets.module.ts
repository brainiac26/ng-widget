import { NgModule } from '@angular/core';
import { TicketsComponent } from './tickets.component';
import {CoreModule} from '../core/core.module';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [TicketsComponent],
  imports: [
    CoreModule,
    SharedModule
  ]
})
export class TicketsModule { }
