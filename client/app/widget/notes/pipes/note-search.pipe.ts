import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noteSearch'
})
export class NoteSearchPipe implements PipeTransform {

  transform(value: any[], term: string): any[] {
    if (!value || !Array.isArray(value) || !value.length || !term) return value;
    return value.filter(val => {
      return (val.title && val.title.toLowerCase().includes(term.toLowerCase()))
        || (val.body && val.body.toLowerCase().includes(term.toLowerCase()))
        || (val.checklist && val.checklist.some(item => item.text.includes(term.toLowerCase())));
    })
  }

}
