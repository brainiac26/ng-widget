import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {fromEvent} from 'rxjs';
import {mergeMap, takeUntil} from 'rxjs/internal/operators';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.css']
})
export class CanvasComponent implements OnInit {

  @Input() url: string;

  @Input() color: any;

  @Input() eraser: boolean;

  @Output() drawingChanged = new EventEmitter<any>();

  private canvas: HTMLCanvasElement;

  private ctx: CanvasRenderingContext2D;

  private infiniteX = Infinity;

  private infiniteY = Infinity;

  private offset: any;

  constructor(private elRef: ElementRef) { }

  ngOnInit(): void {}

  ngAfterViewInit() {
    this.initialize(this.elRef.nativeElement);
    this.startPainting();
  }

  private initialize(mountPoint: HTMLElement) {
    this.canvas = mountPoint.querySelector('canvas') as HTMLCanvasElement;
    this.ctx = this.canvas.getContext('2d');
    this.canvas.width = 250;
    this.canvas.height = 307;
    this.ctx.lineJoin = 'round';
    this.ctx.lineCap = 'round';

    this.ctx.fillStyle = "#fff";
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

    if (this.url) {
      const img = new Image();
      img.crossOrigin = 'anonymous';
      img.src = this.url;
      img.onload = () => {
        this.ctx.drawImage(img, 0, 0);
      };
    }
  }

  private startPainting() {
    const move$ = fromEvent<MouseEvent>(this.canvas, 'mousemove');
    const down$ = fromEvent<MouseEvent>(this.canvas, 'mousedown');
    const up$ = fromEvent<MouseEvent>(this.canvas, 'mouseup');
    const paints$ = down$.pipe(
      mergeMap(down => move$.pipe(takeUntil(up$)))
    );

    down$.subscribe();

    this.offset = getOffset(this.canvas);

    paints$.subscribe((event) => {
      this.paint({ eventClientX: event.clientX, eventClientY: event.clientY });

      this.canvas.toBlob(blob => {
        this.drawingChanged.next(blob);
      });
    });
  }

  paint({ eventClientX, eventClientY }) {
    let clientX, clientY;
    if (this.eraser) {
      this.ctx.lineWidth = 20;
      this.ctx.strokeStyle = '#fff';
      clientX = eventClientX - this.offset.left + 10;
      clientY = eventClientY - this.offset.top - 100;
    } else {
      this.ctx.lineWidth = 2;
      this.ctx.strokeStyle = this.color;
      clientX = eventClientX - this.offset.left;
      clientY = eventClientY - this.offset.top - 90;
    }

    this.ctx.beginPath();
    if (Math.abs(this.infiniteX - clientX) < 100 && Math.abs(this.infiniteY - clientY) < 100) {
      this.ctx.moveTo(this.infiniteX, this.infiniteY);
    }
    this.ctx.lineTo(clientX, clientY);
    this.ctx.stroke();
    this.infiniteX = clientX;
    this.infiniteY = clientY;
  }

}

function getOffset(el: HTMLElement) {
  const rect = el.getBoundingClientRect();

  return {
    top: rect.top + document.body.scrollTop,
    left: rect.left + document.body.scrollLeft
  };

}
