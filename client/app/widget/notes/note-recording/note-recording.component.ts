import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faMicrophone, faStop} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import * as RecordRTC from 'recordrtc';
import {FormGroup} from '@angular/forms';

@Component({
  selector: 'app-note-recording',
  templateUrl: './note-recording.component.html',
  styleUrls: ['./note-recording.component.css']
})
export class NoteRecordingComponent implements OnInit {

  @Input() url: string;

  @Input() formGroup: FormGroup;

  @Output() audioCreated = new EventEmitter<any>();

  recording: boolean;

  blobUrl: any;

  interval: any;

  recordingTimer = ``;

  recordWebRTC: any;

  mediaRecordStream: any;

  options: any = {
    type: 'audio',
    mimeType: 'audio/webm'
  };

  constructor(private library: FaIconLibrary) {
    this.library.addIcons(faMicrophone, faStop);
  }

  ngOnInit(): void {
  }

  clearStream(stream: any) {
    try {
      stream.getAudioTracks().forEach(track => track.stop());
      stream.getVideoTracks().forEach(track => track.stop());
    } catch (error) {
      //stream error
    }
  }

  getUrl() {
    return this.blobUrl || this.url;
  }

  startRecording() {
    this.recording = true;

    navigator.mediaDevices.getUserMedia({ audio: true }).then(stream => {
      this.startRTC(stream);
    }).catch(error => {
      console.log(error);
    })
  }

  stopRecording() {
    this.recording = false;

    this.recordWebRTC.stop((blob) => {
      this.stopCountdown();
      this.audioCreated.next(blob);
      this.blobUrl = URL.createObjectURL(blob);
    })
  }

  startRTC(stream: any) {
    this.recordWebRTC = new RecordRTC.StereoAudioRecorder(stream, this.options);
    this.mediaRecordStream = stream;
    this.blobUrl = null;
    this.recordWebRTC.record();
    this.startCountdown();
  }

  startCountdown() {
    this.recordingTimer = `00:00`;
    this.interval = setInterval(() => {

      let timer: any = this.recordingTimer;
      timer = timer.split(':');
      let minutes = +timer[0];
      let seconds = +timer[1];

      if (minutes === 10) {
        this.recordWebRTC.stopRecording();
        clearInterval(this.interval);
        return
      }
      ++seconds;
      if (seconds >= 59) {
        ++minutes;
        seconds = 0;
      }

      this.recordingTimer = `0${minutes}:${seconds < 10 ? '0' + seconds : seconds}`;
    }, 1000);
  }

  stopCountdown() {
    this.clearStream(this.mediaRecordStream);
    this.recordWebRTC = null;
    this.recordingTimer = ``;
    this.mediaRecordStream = null;
    clearInterval(this.interval);
  }

}
