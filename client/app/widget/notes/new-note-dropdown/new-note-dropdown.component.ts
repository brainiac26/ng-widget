import {Component, OnInit} from '@angular/core';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {Router} from '@angular/router';

@Component({
  selector: 'app-new-note-dropdown',
  templateUrl: './new-note-dropdown.component.html',
  styleUrls: ['./new-note-dropdown.component.css']
})
export class NewNoteDropdownComponent implements OnInit {

  options = [
    {
      caption: 'Note',
      value: 'text',
      icon: 'icon-pen-alt'
    },
    {
      caption: 'Checklist',
      value: 'checklist',
      icon: 'icon-list'
    },
    {
      caption: 'Voice recording',
      value: 'recording',
      icon: 'icon-microphone'
    },
    {
      caption: 'Handwriting',
      value: 'drawing',
      icon: 'icon-paint'
    }
  ];

  constructor(private router: Router, private library: FaIconLibrary) {
    this.library.addIcons(faPlus);
  }

  ngOnInit(): void {
  }

  selectOption(value: string) {
    this.router.navigate([{ outlets: { outlet: ['edit-note'] } }], { queryParams: { type: value }});
  }

}
