import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-note-list',
  templateUrl: './note-list.component.html',
  styleUrls: ['./note-list.component.css']
})
export class NoteListComponent implements OnInit {

  @Input() notes: any[];

  height: number;

  constructor() { }

  ngOnInit(): void {
  }

  getHeight() {
    const height = this.notes.slice(0,5)
      .map(note => this.getNoteHeight(note))
      .reduce((acc: number, curr: number) => acc + curr, 0);
    return height + 'px';
  }

  getNoteHeight(note) {
    switch(note.type) {
      case 'text':
      case 'recording':
        return note.body ? 74 : 55;
      case 'checklist':
        return note.checklist.length === 1 ? 74 : 88;
      case 'drawing':
        return 55;
      default:
        return 0;
    }
  }

}
