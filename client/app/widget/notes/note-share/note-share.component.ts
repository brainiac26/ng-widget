import {Component, ElementRef, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-note-share',
  templateUrl: './note-share.component.html',
  styleUrls: ['./note-share.component.css']
})
export class NoteShareComponent implements OnInit {

  showDropdown: boolean;

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if(!this.eRef.nativeElement.contains(event.target)) {
      this.showDropdown = false;
    }
  }

  constructor(private eRef: ElementRef) { }

  ngOnInit(): void {
  }

  toggle() {
    this.showDropdown = !this.showDropdown;
  }

}
