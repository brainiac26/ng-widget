import {Component, OnDestroy, OnInit} from '@angular/core';
import {NotesService} from '../services/notes.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit, OnDestroy {

  notes: any[];

  subscription: Subscription;

  sortOptions = [
    { caption: 'Title', field: 'title', caseInsensitive: true },
    { caption: 'Modified date (newest first)', reverse: true, field: 'modifiedTimestamp' },
    { caption: 'Modified date (oldest first)', field: 'modifiedTimestamp' },
    { caption: 'Created date (newest first)', reverse: true, field: 'createdTimestamp' },
    { caption: 'Created date (oldest first)', field: 'createdTimestamp' },
  ];

  sortOption: any;

  searchTerm: string;

  constructor(private notesService: NotesService, private router: Router) { }

  ngOnInit(): void {
    this.sortOption = this.notesService.getSortOption() || this.sortOptions[1];

    this.subscription = this.notesService.updateNotes.subscribe((notes: any[]) => {
      if (notes) {
        this.notes = notes;
      }
    })
  }

  goBack() {
    this.router.navigate(['']);
  }

  onSelectSortOption(option) {
    this.sortOption = option;
    this.notesService.setSortOption(option);
  }

  onUpdateSearch(searchTerm) {
    this.searchTerm = searchTerm;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
