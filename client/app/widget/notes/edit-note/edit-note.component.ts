import { Component, OnInit } from '@angular/core';
import {faTimes, faArrowsAltV, faEraser, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {ActivatedRoute, Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotesService} from '../../services/notes.service';
import {switchMap, tap} from 'rxjs/internal/operators';
import {FilesService} from '../../services/files.service';
import {UserService} from '../../services/user.service';
import {Observable, Subject} from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-edit-note',
  templateUrl: './edit-note.component.html',
  styleUrls: ['./edit-note.component.css']
})
export class EditNoteComponent implements OnInit {

  type: string;

  isEdit: boolean;

  note: any;

  saving: boolean;

  saved: boolean;

  noteForm: FormGroup;

  initialFormValue: any;

  // audio

  audioBlob: any;

  // drawing

  drawingBlob: any;

  color = '#000';

  eraser = false;

  // confirm exit

  showExitConfirmation: boolean;

  confirmExitManager = new Subject<boolean>();

  confirmExit: Observable<boolean>;

  // confirm delete

  showDeleteConfirmation: boolean;

  // monitoring

  changes: any;

  constructor(private library: FaIconLibrary,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private notesService: NotesService,
              private router: Router,
              private filesService: FilesService,
              private userService: UserService) {
    this.library.addIcons(faTimes, faArrowsAltV, faEraser, faSpinner);

    this.confirmExit = this.confirmExitManager.asObservable();

    this.noteForm = this.fb.group({
      userId: this.userService.userId,
      title: '',
      body: '',
      type: '',
      checklist: this.fb.array([]),
      recordingUrl: '',
      drawingUrl: ''
    })
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.type = params.get('type');

      const id = params.get('id');
      this.isEdit = !!id;

      if (this.isEdit) {
        // edit note
        this.note = this.notesService.getSavedNote(id);
        this.noteForm.patchValue({
          userId: this.note.userId,
          title: this.note.title,
          body: this.note.body,
          type: this.note.type,
          recordingUrl: this.note.recordingUrl,
          drawingUrl: this.note.drawingUrl
        });

        if (this.note.checklist) {
          this.note.checklist.forEach(item => this.onAddChecklistItem(item.text, item.done));
        }
      } else {
        // create new note
        this.noteForm.patchValue({type: this.type});
      }

      this.initialFormValue = this.noteForm.value;
    });
  }

  getChanges() {
    const newFormValue = this.noteForm.value;

    const changes = [];

    if (this.isEdit) {
      _.map(this.initialFormValue, (value, key) => {
        const oldValue = JSON.stringify(value);
        const newValue = JSON.stringify(newFormValue[key]);
        if (oldValue !== newValue) {
          changes.push({
            fieldName: key,
            oldValue,
            newValue
          });
        }
      });

      if (this.audioBlob) {
        changes.push({
          fieldName: 'recordingUrl',
          oldValue: this.noteForm.get('recordingUrl').value,
          newValue: this.noteForm.get('recordingUrl').value
        });
      }

      if (this.drawingBlob) {
        changes.push({
          fieldName: 'drawingUrl',
          oldValue: this.noteForm.get('drawingUrl').value,
          newValue: this.noteForm.get('drawingUrl').value
        });
      }
    } else {
      _.map(newFormValue, (value, key) => {
        changes.push({
          fieldName: key,
          oldValue: '',
          newValue: JSON.stringify(value)
        })
      })
    }

    return changes;
  }

  goBack() {
    this.router.navigate([{ outlets: { outlet: ['notes'] } }]);
  }

  canDeactivate() {
    if (this.saved || !this.isEdit || this.noteNotChanged()) {
      return true;
    } else {
      this.showExitConfirmation = true;
      return this.confirmExit;
    }
  }

  noteNotChanged() {
    const initialNote = this.note;
    const editedNote = this.noteForm.value;

    switch (this.type) {
      case 'text':
        return initialNote.title === editedNote.title &&
          initialNote.body === editedNote.body;
      case 'checklist':
        return initialNote.title === editedNote.title &&
          initialNote.checklist.length === editedNote.checklist.length &&
          initialNote.checklist.every((initialItem, index) =>
            initialItem.text === editedNote.checklist[index].text && initialItem.done === editedNote.checklist[index].done
          );
      case 'drawing':
        return initialNote.title === editedNote.title && !this.drawingBlob;
      case 'recording':
        return initialNote.title === editedNote.title && initialNote.body === editedNote.body && !this.audioBlob;
    }
  }

  getDrawingUrl() {
    return this.noteForm.get('drawingUrl').value;
  }

  getRecordingUrl() {
    return this.noteForm.get('recordingUrl').value;
  }

  onChangeColor(color: string) {
    this.color = color;
    this.eraser = false;
  }

  onAddChecklistItem(text: string, done?: boolean) {
    const checklist = this.noteForm.get('checklist') as FormArray;
    checklist.push(this.createChecklistItem(text, done));
  }

  onRemoveChecklistItem(index: number) {
    const checklist = this.noteForm.get('checklist') as FormArray;
    checklist.removeAt(index);
  }

  onDelete() {
    if (this.isEdit) {
      this.showDeleteConfirmation = true;
    } else {
      this.router.navigate([{ outlets: { outlet: ['notes'] } }]);
    }
  }

  onConfirmExit() {
    this.confirmExitManager.next(true);
    this.showExitConfirmation = false;
  }

  onCancelExit() {
    this.confirmExitManager.next(false);
    this.showExitConfirmation = false;
  }

  onConfirmDelete() {
    this.showDeleteConfirmation = false;
    this.notesService.delete(this.note.id)
      .subscribe(() => {
        this.router.navigate([{ outlets: { outlet: ['notes'] } }]);
      })
  }

  onCancelDelete() {
    this.showDeleteConfirmation = false;
  }

  createChecklistItem(text: string, done?: boolean) {
    return this.fb.group({
      text: [text, Validators.required],
      done: done || false
    });
  }

  toggleEraser() {
    this.eraser = !this.eraser;
  }

  onAudioCreated(blob: any) {
    this.audioBlob = blob;
  }

  onDrawingChanged(blob: any) {
    this.drawingBlob = blob;
  }

  onSetReminder() {
    this.router.navigate([{ outlets: { outlet: ['reminder'] } }], { queryParams: { noteId: this.note && this.note.id }});
  }

  isFilled() {
    const note = this.noteForm.value;

    switch(this.type) {
      case 'text':
        return note.title || note.body;
      case 'checklist':
        return note.checklist.length;
      case 'recording':
        return this.audioBlob;
      case 'drawing':
        return this.drawingBlob;
    }
  }

  save() {
    if (this.saving) return;

    if (!this.isEdit && !this.isFilled()) {
      return this.router.navigate([{ outlets: { outlet: ['notes'] } }]);
    }

    this.saving = true;

    let request;
    if (this.type === 'recording' && this.audioBlob) {
      request = this.saveNoteWithRecording();
    } else if (this.type === 'drawing' && this.drawingBlob) {
      request = this.saveNoteWithDrawing();
    } else {
      request = this.makeSaveRequest();
    }
    request
      .subscribe(() => {
        this.saving = false;
        this.saved = true;
        this.router.navigate([{ outlets: { outlet: ['notes'] } }]);
      })
  }

  saveNoteWithDrawing() {
    return this.uploadFile(this.drawingBlob, this.getDrawingUrl())
      .pipe(
        tap((res: any) => {
          this.noteForm.patchValue({drawingUrl: res.url});
        }),
        switchMap(() => this.makeSaveRequest())
      )
  }

  saveNoteWithRecording() {
    return this.uploadFile(this.audioBlob, this.getRecordingUrl())
      .pipe(
        tap((res: any) => {
          this.noteForm.patchValue({recordingUrl: res.url});
        }),
        switchMap(() => this.makeSaveRequest())
      )
  }

  makeSaveRequest() {
    const data = this.noteForm.value;
    return this.isEdit ?
      this.notesService.update(this.note.id, data, this.getChanges()) :
      this.notesService.create(data, this.getChanges());
  }

  uploadFile(file, url) {
    return this.filesService.upload(file, url);
  }

}
