import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faTrash, faUpload, faBell, faImage, faPaperclip} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-note-actions',
  templateUrl: './note-actions.component.html',
  styleUrls: ['./note-actions.component.css']
})
export class NoteActionsComponent implements OnInit {

  @Input() isEdit: boolean;

  @Output() delete = new EventEmitter();

  @Output() setReminder = new EventEmitter();

  constructor(private library: FaIconLibrary) {
    this.library.addIcons(faTrash, faUpload, faBell, faImage, faPaperclip);
  }

  ngOnInit(): void {
  }

}
