import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faChevronDown} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-reminder-repeat-dropdown',
  templateUrl: './reminder-repeat-dropdown.component.html',
  styleUrls: ['./reminder-repeat-dropdown.component.css']
})
export class ReminderRepeatDropdownComponent implements OnInit {

  @Input() value: string;

  @Output() select = new EventEmitter();

  option: any;

  options = [
    {
      value: 'never',
      caption: 'Never'
    },
    {
      value: 'daily',
      caption: 'Daily'
    },
    {
      value: 'weekly',
      caption: 'Weekly'
    },
    {
      value: 'monthly',
      caption: 'Monthly'
    },
    {
      value: 'yearly',
      caption: 'Yearly'
    }
  ];

  constructor(private library: FaIconLibrary) {
    this.library.addIcons(faChevronDown);
  }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.value) {
      this.option = this.options.find(option => option.value === this.value);
    }
  }

}
