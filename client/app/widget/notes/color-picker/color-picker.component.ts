import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-color-picker',
  templateUrl: './color-picker.component.html',
  styleUrls: ['./color-picker.component.css']
})
export class ColorPickerComponent implements OnInit {

  @Input('color') selectedColor: string;

  @Output() changeColor = new EventEmitter<string>();

  colors = [
    '#000000', '#003071', '#040071', '#2C0074', '#7C004C', '#A20000', '#8B3B00', '#6F6700', '#1E6F00', '#005D48',
    '#343434', '#1242AF', '#0D1296', '#430091', '#91096D', '#D02424', '#CE5F00', '#CEAE00', '#77B502', '#009F92',
    '#5D5D5D', '#3874E6', '#3037D8', '#5126C6' ,'#D83599', '#FF1515', '#FF6C00', '#FFCE00', '#A0DD18', '#13D3CC',
    '#A0A0A0', '#6BA1F8', '#6B70F8', '#816BF8' ,'#F86BB7', '#F86B6B', '#F8A76B', '#F8D76B', '#B9EB6A', '#7AEFD4',
    '#CBCBCB', '#B1D2FF', '#B1B3FF', '#C9B1FF' ,'#FFB1F0', '#FFB1B1', '#FFCFB1', '#FFEAB1', '#DDFF91', '#A7F8DF',
    '#FFFFFF', '#EAF3FD', '#EAEAFD', '#F4EAFD' ,'#FDEAF9', '#FDEAEA', '#FDF2EA', '#FFF5CB', '#F0FFBF', '#D0FDE3'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
