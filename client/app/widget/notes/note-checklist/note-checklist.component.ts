import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {FormArray, FormGroup} from '@angular/forms';

@Component({
  selector: 'app-note-checklist',
  templateUrl: './note-checklist.component.html',
  styleUrls: ['./note-checklist.component.css']
})
export class NoteChecklistComponent implements OnInit {

  @Input() formGroup: FormGroup;
  @Output() add = new EventEmitter<string>();
  @Output() delete = new EventEmitter<string>();

  items: any[] = [];

  newItem: string;

  constructor() { }

  ngOnInit(): void {
  }

  addItem() {
    this.add.next(this.newItem);
    this.newItem = '';
  }

  dropItem(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.formGroup.get('checklist').value, event.previousIndex, event.currentIndex);
  }

  getChecklist() {
    return (<FormArray>this.formGroup.get('checklist')).controls;
  }

  removeItem(index) {
    this.delete.next(index);
  }

  toggle(index) {
    this.items = this.items.map((item, i) => {
      if (i !== index) return item;
      item.done = !item.done;
      return item;
    })
  }

}
