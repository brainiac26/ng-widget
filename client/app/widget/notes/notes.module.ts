import { NgModule } from '@angular/core';
import {CoreModule} from '../core/core.module';
import {SharedModule} from '../shared/shared.module';

import {NotesComponent} from './notes.component';
import {EditNoteComponent} from './edit-note/edit-note.component';
import {NewNoteDropdownComponent} from './new-note-dropdown/new-note-dropdown.component';
import {NoteComponent} from './note/note.component';
import {NoteActionsComponent} from './note-actions/note-actions.component';
import {NoteChecklistComponent} from './note-checklist/note-checklist.component';
import {NoteListComponent} from './note-list/note-list.component';
import {NoteRecordingComponent} from './note-recording/note-recording.component';
import {NoteShareComponent} from './note-share/note-share.component';
import {CanvasComponent} from './canvas/canvas.component';
import {ColorPickerComponent} from './color-picker/color-picker.component';
import {NoteSearchPipe} from './pipes/note-search.pipe';
import {SetReminderComponent} from './set-reminder/set-reminder.component';
import {ReminderRepeatDropdownComponent} from './reminder-repeat-dropdown/reminder-repeat-dropdown.component';



@NgModule({
  declarations: [
    NotesComponent,
    EditNoteComponent,
    NewNoteDropdownComponent,
    NoteComponent,
    NoteActionsComponent,
    NoteChecklistComponent,
    NoteListComponent,
    NoteRecordingComponent,
    NoteShareComponent,
    CanvasComponent,
    ColorPickerComponent,
    NoteSearchPipe,
    SetReminderComponent,
    ReminderRepeatDropdownComponent
  ],
  imports: [
    CoreModule,
    SharedModule
  ],
  exports: [NotesComponent, EditNoteComponent, SetReminderComponent]
})
export class NotesModule { }
