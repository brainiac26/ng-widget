import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {faCircle, faCheck} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';

@Component({
  selector: 'app-note',
  templateUrl: './note.component.html',
  styleUrls: ['./note.component.css']
})
export class NoteComponent implements OnInit {

  @Input() note: any;

  constructor(private library: FaIconLibrary, private router: Router) {
    this.library.addIcons(faCircle, faCheck);
  }

  ngOnInit(): void {}

  getTitle() {
    switch(this.note.type) {
      case 'drawing':
        return 'Handwriting';
      case 'checklist':
        return 'Checklist';
      case 'text':
        return 'Note';
      case 'recording':
        return 'Voice';
      default:
        return '';
    }
  }

  getIcon() {
    switch(this.note.type) {
      case 'drawing':
        return 'icon-paint';
      case 'checklist':
        return 'icon-list';
      case 'text':
        return 'icon-pen-alt';
      case 'recording':
        return 'icon-microphone';
      default:
        return 'icon-pen-alt';
    }
  }

  edit() {
    this.router.navigate(
      [{ outlets: { outlet: ['edit-note'] } }],
      { queryParams: { type: this.note.type, id: this.note.id }}
    );
  }

}
