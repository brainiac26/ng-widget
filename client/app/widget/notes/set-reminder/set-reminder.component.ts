import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {faCalendar} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {FormBuilder, FormGroup} from '@angular/forms';
import {NotesService} from '../../services/notes.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-set-reminder',
  templateUrl: './set-reminder.component.html',
  styleUrls: ['./set-reminder.component.css']
})
export class SetReminderComponent implements OnInit {

  reminderForm: FormGroup;

  noteId: string;

  saving = false;

  constructor(private library: FaIconLibrary,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private notesService: NotesService,
              private location: Location) {
    this.library.addIcons(faCalendar);

    this.reminderForm = this.fb.group({
      date: null,
      time: '',
      repeat: 'never'
    });
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.noteId = params.get('noteId');
    });
  }

  goBack() {
    this.location.back();
  }

  getValue(field) {
    return this.reminderForm.get(field).value;
  }

  onSelectDate(date) {
    this.setValue('date', date ? date.getTime() : null);
  }

  setValue(field: string, value: any) {
    this.reminderForm.patchValue({ [field]: value });
  }

  save() {
    this.saving = true;
    this.notesService.createReminder(this.reminderForm.value, this.noteId)
      .subscribe(() => {
        this.saving = false;
      }, (err) => {
        this.saving = false;
        console.log(err);
      });
  }

}
