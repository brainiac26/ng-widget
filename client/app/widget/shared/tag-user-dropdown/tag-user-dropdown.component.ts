import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-tag-user-dropdown',
  templateUrl: './tag-user-dropdown.component.html',
  styleUrls: ['./tag-user-dropdown.component.css']
})
export class TagUserDropdownComponent implements OnInit {

  users: any[];

  constructor(private usersService: UsersService) {
  }

  ngOnInit(): void {
    this.users = this.usersService.users;
  }

}
