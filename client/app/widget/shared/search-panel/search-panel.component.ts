import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-search-panel',
  templateUrl: './search-panel.component.html',
  styleUrls: ['./search-panel.component.css']
})
export class SearchPanelComponent implements OnInit {

  @Input() searchTerm: string;

  @Output() updateSearch = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

}
