import { NgModule } from '@angular/core';
import {MenuActionsComponent} from './menu-actions/menu-actions.component';
import {CoreModule} from '../core/core.module';
import { SearchPanelComponent } from './search-panel/search-panel.component';
import { FilterDropdownComponent } from './filter-dropdown/filter-dropdown.component';
import { ItemsComponent } from './items/items.component';
import { ItemsListComponent } from './items-list/items-list.component';
import { ItemsSearchPipe } from './pipes/items-search.pipe';
import { ItemComponent } from './item/item.component';
import { NewItemComponent } from './new-item/new-item.component';
import { PriorityDropdownComponent } from './priority-dropdown/priority-dropdown.component';
import { TextboxComponent } from './textbox/textbox.component';
import { AttachDropdownComponent } from './attach-dropdown/attach-dropdown.component';
import { SelectUserDropdownComponent } from './select-user-dropdown/select-user-dropdown.component';
import { TagUserDropdownComponent } from './tag-user-dropdown/tag-user-dropdown.component';
import { StatusDropdownComponent } from './status-dropdown/status-dropdown.component';
import { UsersSearchPipe } from './pipes/users-search.pipe';
import { EditItemComponent } from './edit-item/edit-item.component';
import { TicketTypeDropdownComponent } from './ticket-type-dropdown/ticket-type-dropdown.component';
import { DatepickerDropdownComponent } from './datepicker-dropdown/datepicker-dropdown.component';
import {DatepickerInputComponent} from './datepicker-input/datepicker-input.component';
import {OptionsDropdownComponent} from './options-dropdown/options-dropdown.component';
import { ConfirmExitComponent } from './confirm-exit/confirm-exit.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ConfirmDeleteComponent } from './confirm-delete/confirm-delete.component';
import { AudioComponent } from './audio/audio.component';

@NgModule({
  declarations: [
    MenuActionsComponent,
    SearchPanelComponent,
    FilterDropdownComponent,
    ItemsComponent,
    ItemsListComponent,
    ItemsSearchPipe,
    ItemComponent,
    NewItemComponent,
    PriorityDropdownComponent,
    TextboxComponent,
    AttachDropdownComponent,
    SelectUserDropdownComponent,
    TagUserDropdownComponent,
    StatusDropdownComponent,
    UsersSearchPipe,
    EditItemComponent,
    TicketTypeDropdownComponent,
    DatepickerDropdownComponent,
    DatepickerInputComponent,
    OptionsDropdownComponent,
    ConfirmExitComponent,
    ConfirmDialogComponent,
    ConfirmDeleteComponent,
    AudioComponent
  ],
  imports: [
    CoreModule
  ],
  exports: [
    MenuActionsComponent,
    SearchPanelComponent,
    FilterDropdownComponent,
    ItemsComponent,
    OptionsDropdownComponent,
    DatepickerInputComponent,
    ConfirmExitComponent,
    ConfirmDeleteComponent,
    AudioComponent,
    AttachDropdownComponent
  ]
})
export class SharedModule { }
