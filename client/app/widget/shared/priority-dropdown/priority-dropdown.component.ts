import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-priority-dropdown',
  templateUrl: './priority-dropdown.component.html',
  styleUrls: ['./priority-dropdown.component.css']
})
export class PriorityDropdownComponent implements OnInit {

  @Input() priority: string;

  @Output() select = new EventEmitter();

  priorities = [ 'high', 'normal', 'low' ];

  constructor(private library: FaIconLibrary) {
    this.library.addIcons(faChevronDown);
  }

  ngOnInit(): void {}

}
