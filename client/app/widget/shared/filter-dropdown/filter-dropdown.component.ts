import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-filter-dropdown',
  templateUrl: './filter-dropdown.component.html',
  styleUrls: ['./filter-dropdown.component.css']
})
export class FilterDropdownComponent implements OnInit {

  @Input() options: any[];

  @Input() selectedOption: any;

  @Output() select = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {
  }

  isSelected(option) {
    return option.caption === this.selectedOption.caption;
  }

}
