import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-select-user-dropdown',
  templateUrl: './select-user-dropdown.component.html',
  styleUrls: ['./select-user-dropdown.component.css']
})
export class SelectUserDropdownComponent implements OnInit {

  @Input() showDropdownIcon: boolean;

  @Input() userId: string;

  @Output() select = new EventEmitter();

  users: any[];

  user: any;

  searchTerm: string;

  constructor(private library: FaIconLibrary, private usersService: UsersService) {
    this.library.addIcons(faChevronDown);
  }

  ngOnInit(): void {
    this.users = this.usersService.users;
  }

  ngOnChanges() {
    if (!this.users) {
      this.users = this.usersService.users;
    }

    this.user = this.usersService.findUser(this.userId);
  }

}
