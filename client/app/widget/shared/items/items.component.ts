import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {TasksService} from '../../services/tasks.service';
import {faPlus} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {TicketsService} from '../../services/tickets.service';

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  @Input() type: string;
  @Input() sortOptions: any[];

  items: any[];

  service: any;

  subscription: Subscription;

  sortOption: any;

  searchTerm: string;

  constructor(private tasksService: TasksService, private ticketsService: TicketsService, private library: FaIconLibrary, private router: Router) {
    this.library.addIcons(faPlus);
  }

  ngOnInit(): void {
    this.service = this.type === 'task' ? this.tasksService : this.ticketsService;
    this.sortOption = this.service.getSortOption() || this.sortOptions[1];

    this.subscription = this.service.updateItems.subscribe((items: any[]) => {
      if (items) {
        this.items = items;
      }
    })
  }

  add() {
    this.router.navigate([{ outlets: { outlet: ['new-item'] } }], { queryParams: { type: this.type }});
  }

  goBack() {
    this.router.navigate(['']);
  }

  onSelectSortOption(option) {
    this.sortOption = option;
    this.tasksService.setSortOption(option);
  }

  onUpdateSearch(searchTerm) {
    this.searchTerm = searchTerm;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
