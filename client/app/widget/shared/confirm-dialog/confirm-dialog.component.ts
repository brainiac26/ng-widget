import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.css']
})
export class ConfirmDialogComponent implements OnInit {

  @Output() confirm = new EventEmitter();

  @Output() cancel = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
