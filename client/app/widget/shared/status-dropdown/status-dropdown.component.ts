import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-status-dropdown',
  templateUrl: './status-dropdown.component.html',
  styleUrls: ['./status-dropdown.component.css']
})
export class StatusDropdownComponent implements OnInit, OnChanges {

  @Input() status: string;

  @Output() select = new EventEmitter();

  options = [
    {
      value: 'new',
      caption: 'New',
      icon: 'icon-clock'
    },
    {
      value: 'in progress',
      caption: 'In progress',
      icon: 'icon-clock'
    },
    {
      value: 'completed',
      caption: 'Completed',
      icon: 'icon-ok'
    }
  ];

  option: any;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    this.option = this.options.find(option => option.value === this.status);
  }

}
