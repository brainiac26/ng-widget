import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbDate} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-datepicker-dropdown',
  templateUrl: './datepicker-dropdown.component.html',
  styleUrls: ['./datepicker-dropdown.component.css']
})
export class DatepickerDropdownComponent implements OnInit {

  @Input() timestamp: number;

  @Output() selectDate = new EventEmitter();

  date: NgbDate;

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.timestamp) {
      const date = new Date(this.timestamp);
      this.date = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
    }
  }

  onSelectDate(date: NgbDate) {
    this.selectDate.next(new Date(date.year, date.month - 1, date.day));
  }

}
