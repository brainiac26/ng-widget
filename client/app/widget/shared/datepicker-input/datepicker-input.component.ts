import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-datepicker-input',
  templateUrl: './datepicker-input.component.html',
  styleUrls: ['./datepicker-input.component.css']
})
export class DatepickerInputComponent implements OnInit {

  @Input() timestamp: number;

  @Output() selectDate = new EventEmitter();

  format = 'DD/MM/YYYY';

  date: Date;

  dateInput: string = '';

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.timestamp) {
      this.date = new Date(this.timestamp);
      this.dateInput = moment(this.date).format(this.format);
    }
  }

  onModelChange(dateInput) {
    this.dateInput = dateInput;
    if (dateInput.length === 10) {
      const formattedDate = moment(dateInput, this.format);
      if (!formattedDate.isValid()) {
        this.dateInput = '';
        this.date = null;
      } else {
        this.date = formattedDate.toDate();
      }
    } else {
      this.date = null;
    }

    this.selectDate.next(this.date);
  }

  onSelectDate(date) {
    this.dateInput = moment(date).format(this.format);
    this.selectDate.next(date);
  }

}
