import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-attach-dropdown',
  templateUrl: './attach-dropdown.component.html',
  styleUrls: ['./attach-dropdown.component.css']
})
export class AttachDropdownComponent implements OnInit {

  @ViewChild('file') fileEl: ElementRef;

  options = [
    {
      value: 'screenshot',
      caption: 'Screenshot',
      icon: 'icon-photo'
    },
    {
      value: 'file',
      caption: 'File',
      icon: 'icon-image'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(value) {
    if (value === 'screenshot') {

    } else if (value === 'file') {
      this.openFiles();
    }
  }

  openFiles() {
    let event = new MouseEvent('click', {bubbles: true});
    this.fileEl.nativeElement.dispatchEvent(event);
  }

}
