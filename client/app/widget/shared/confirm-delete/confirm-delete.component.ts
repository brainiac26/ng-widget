import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-confirm-delete',
  templateUrl: './confirm-delete.component.html',
  styleUrls: ['./confirm-delete.component.css']
})
export class ConfirmDeleteComponent implements OnInit {

  @Output() confirm = new EventEmitter();

  @Output() cancel = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
