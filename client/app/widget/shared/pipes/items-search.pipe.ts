import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'itemsSearch'
})
export class ItemsSearchPipe implements PipeTransform {

  transform(value: any[], term: string): any[] {
    if (!value || !Array.isArray(value) || !value.length || !term) return value;
    return value.filter(val => {
      return (val.title && val.title.toLowerCase().includes(term.toLowerCase()))
        || (val.description && val.description.toLowerCase().includes(term.toLowerCase()));
    })
  }

}
