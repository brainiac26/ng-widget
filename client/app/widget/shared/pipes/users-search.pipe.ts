import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'usersSearch'
})
export class UsersSearchPipe implements PipeTransform {

  transform(value: any[], term: string): any[] {
    if (!value || !Array.isArray(value) || !value.length || !term) return value;
    return value.filter(val => {
      return (val.email && val.email.toLowerCase().includes(term)) || (val.name && val.name.toLowerCase().includes(term))
        || (val.username && val.username.toLowerCase().includes(term));
    })
  }
}
