import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-confirm-exit',
  templateUrl: './confirm-exit.component.html',
  styleUrls: ['./confirm-exit.component.css']
})
export class ConfirmExitComponent implements OnInit {

  @Output() confirm = new EventEmitter();

  @Output() cancel = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

}
