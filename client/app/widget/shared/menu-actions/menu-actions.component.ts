import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {faChevronLeft, faTimes, faCheck, faSpinner } from '@fortawesome/free-solid-svg-icons';
import {NavigationService} from '../../services/navigation.service';

@Component({
  selector: 'app-menu-actions',
  templateUrl: './menu-actions.component.html',
  styleUrls: ['./menu-actions.component.css']
})
export class MenuActionsComponent implements OnInit {

  @Input() showBack = true;

  @Input() saving: boolean;

  @Input() showSave: boolean;

  @Output() back = new EventEmitter();

  @Output() save = new EventEmitter();

  constructor(private library: FaIconLibrary,
              private navigationService: NavigationService) {
    library.addIcons(faChevronLeft, faTimes, faCheck, faSpinner);
  }

  ngOnInit(): void {
  }

  close() {
    this.navigationService.doClose();
  }

}
