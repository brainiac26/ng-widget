import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-items-list',
  templateUrl: './items-list.component.html',
  styleUrls: ['./items-list.component.css']
})
export class ItemsListComponent implements OnInit {

  @Input() items: any[];

  @Input() type: string;

  constructor() { }

  ngOnInit(): void {
  }

  getHeight() {
    return this.type === 'task' ? 439 + 'px' : 363 + 'px' ;
  }
}
