import {AfterViewInit, Component, ElementRef, Input, OnInit, SecurityContext, ViewChild} from '@angular/core';

declare var GreenAudioPlayer: any;

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.css']
})
export class AudioComponent implements OnInit, AfterViewInit {

  @Input() url: string;

  @ViewChild('audio') audioEl: ElementRef;

  player: any;

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges() {
    if (this.player) {
      this.destroyPlayer();

      this.initPlayer();
    }
  }

  ngAfterViewInit() {
    this.initPlayer();
  }

  initPlayer() {
    this.audioEl.nativeElement.innerHTML = `<audio><source src="${this.url}"></audio>`;
    this.player = new GreenAudioPlayer('.gap-audio');
  }

  destroyPlayer() {
    this.player = null;
    this.audioEl.nativeElement.innerHTML = '';
  }

  ngOnDestroy() {
    this.destroyPlayer();
  }
}
