import { Component, OnInit } from '@angular/core';
import {faCheck, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TasksService} from '../../services/tasks.service';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {UserService} from '../../services/user.service';
import {TicketsService} from '../../services/tickets.service';
import * as _ from 'lodash';

@Component({
  selector: 'app-new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  type: string;

  service: any;

  saving: boolean;

  itemForm: FormGroup;

  constructor(private library: FaIconLibrary,
              private router: Router,
              private route: ActivatedRoute,
              private fb: FormBuilder,
              private userService: UserService,
              private tasksService: TasksService,
              private ticketsService: TicketsService
  ) {
    this.library.addIcons(faCheck, faSpinner);
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {
      this.type = params.get('type');

      this.service = this.type === 'task' ? this.tasksService : this.ticketsService;

      const formValue = {
        createdUserId: this.userService.username || '',
        title: '',
        description: '',
        priority: 'normal',
        status: 'new'
      };

      if (this.type === 'task') {
        formValue['assignedUserId'] = '';
        formValue['dueDate'] = null;
      } else {
        formValue['type'] = 'bug';
      }

      this.itemForm = this.fb.group(formValue);
    });
  }

  getChanges() {
    const newFormValue = this.itemForm.value;

    const changes = [];

    _.map(newFormValue, (value, key) => {
      changes.push({
        fieldName: key,
        oldValue: '',
        newValue: JSON.stringify(value)
      })
    });

    return changes;
  }

  goBack() {
    this.router.navigate([{ outlets: { outlet: [this.type === 'task' ? 'tasks' : 'tickets'] } }]);
  }

  getValue(field) {
    return this.itemForm.get(field).value;
  }

  onSelectDueDate(date) {
    this.setValue('dueDate', date ? date.getTime() : null);
  }

  save() {
    this.saving = true;
    this.service.create(this.itemForm.value, this.getChanges())
      .subscribe(() => {
        this.saving = false;
        this.router.navigate([{ outlets: { outlet: [this.type === 'task' ? 'tasks' : 'tickets'] } }]);
      }, err => {
        this.saving = false;
        console.log(err);
      })
  }

  setValue(field: string, value: any) {
    this.itemForm.patchValue({ [field]: value });
  }

}
