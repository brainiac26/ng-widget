import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-options-dropdown',
  templateUrl: './options-dropdown.component.html',
  styleUrls: ['./options-dropdown.component.css']
})
export class OptionsDropdownComponent implements OnInit {

  @Input() options: any[];

  @Input() showCancel: boolean;

  @Output() select = new EventEmitter();

  option: any;

  constructor() { }

  ngOnInit(): void {
  }

  selectOption(option) {
    this.select.next(option.value);
  }

}
