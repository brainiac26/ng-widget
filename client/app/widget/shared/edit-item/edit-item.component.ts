import { Component, OnInit } from '@angular/core';
import {faCheck, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {TasksService} from '../../services/tasks.service';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';
import {TicketsService} from '../../services/tickets.service';
import * as _ from "lodash";
import {BehaviorSubject, Observable, Subject} from 'rxjs';

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.css']
})
export class EditItemComponent implements OnInit {

  type: string;

  item: any;

  service: any;

  itemForm: FormGroup;

  initialFormValue:  any;

  saving: boolean;

  saved: boolean;

  showExitConfirmation: boolean;

  showDeleteConfirmation: boolean;

  confirmExitManager = new Subject<boolean>();

  confirmExit: Observable<boolean>;

  constructor(private router: Router,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private library: FaIconLibrary,
              private tasksService: TasksService,
              private ticketsService: TicketsService) {
    this.library.addIcons(faCheck, faSpinner);

    this.confirmExit = this.confirmExitManager.asObservable();
  }

  ngOnInit(): void {
    this.route.queryParamMap.subscribe(params => {

      const id = params.get('id');
      this.type = params.get('type');

      this.service = this.type === 'task' ? this.tasksService : this.ticketsService;

      this.item = this.service.getSavedItem(id);

      const formValue = {
        createdUserId: this.item.createdUserId,
        status: this.item.status,
        title: this.item.title,
        description: this.item.description,
        priority: this.item.priority
      };

      if (this.type === 'task') {
        formValue['assignedUserId'] = this.item.assignedUserId;
        formValue['dueDate'] = this.item.dueDate;
      } else {
        formValue['type'] = this.item.type;
      }

      this.itemForm = this.fb.group(formValue);
      this.initialFormValue = this.itemForm.value;
    });
  }

  canDeactivate(): boolean | Observable<boolean> {
    if (this.saved || !this.isItemChanged()) {
      return true;
    } else {
      this.showExitConfirmation = true;
      return this.confirmExit;
    }
  }

  delete() {
    this.showDeleteConfirmation = true;
  }

  getChanges() {
    const newFormValue = this.itemForm.value;

    const changes = [];

    _.map(this.initialFormValue, (value, key) => {
      const oldValue = JSON.stringify(value);
      const newValue = JSON.stringify(newFormValue[key]);
      if (oldValue !== newValue) {
        changes.push({
          fieldName: key,
          oldValue,
          newValue
        });
      }
    });

    return changes;
  }

  goBack() {
    this.router.navigate([{ outlets: { outlet: [this.type === 'task' ? 'tasks' : 'tickets'] } }]);
  }

  getValue(field) {
    return this.itemForm.get(field).value;
  }

  isItemChanged()  {
    return !_.isEqual(this.initialFormValue, this.itemForm.value);
  }

  onConfirmExit() {
    this.confirmExitManager.next(true);
    this.showExitConfirmation = false;
  }

  onCancelExit() {
    this.confirmExitManager.next(false);
    this.showExitConfirmation = false;
  }

  onConfirmDelete() {
    this.showDeleteConfirmation = false;
    this.service.delete(this.item.id)
      .subscribe(() => {
        this.goBack();
      }, err => {
        console.log(err);
      })
  }

  onCancelDelete() {
    this.showDeleteConfirmation = false;
  }

  onSelectDueDate(date) {
    this.setValue('dueDate', date ? date.getTime() : null);
  }

  save() {
    this.saving = true;
    this.service.update(this.item.id, this.itemForm.value, this.getChanges())
      .subscribe(() => {
        this.saving = false;
        this.saved = true;
        this.router.navigate([{ outlets: { outlet: [this.type === 'task' ? 'tasks' : 'tickets'] } }]);
      }, err => {
        this.saving = false;
        console.log(err);
      })
  }

  setValue(field: string, value: any) {
    this.itemForm.patchValue({ [field]: value });
  }

}
