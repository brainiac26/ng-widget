import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UsersService} from '../../services/users.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() type: string;

  @Input() item: any;

  ticketTypes = [
    {
      value: 'bug',
      caption: 'Bug',
      icon: 'icon-bug'
    },
    {
      value: 'feature-request',
      caption: 'Feature request',
      icon: 'icon-new'
    },
    {
      value: 'how-to',
      caption: 'How to',
      icon: 'icon-new'
    },
    {
      value: 'other',
      caption: 'Other',
      icon: 'icon-tools'
    }
  ];

  constructor(private usersService: UsersService, private router: Router) { }

  ngOnInit(): void {
  }

  getAssignedUsername() {
    return this.usersService.findUser(this.item.assignedUserId).name;
  }

  getTicketType(value) {
    return this.ticketTypes.find(type => type.value === value);
  }

  view() {
    this.router.navigate([{
      outlets: {
        outlet: ['edit-item']
      }
    }], {
      queryParams: {
        id: this.item.id,
        type: this.type
      }
    });
  }

}
