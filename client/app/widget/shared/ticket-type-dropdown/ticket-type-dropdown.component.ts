import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {faChevronDown} from '@fortawesome/free-solid-svg-icons';
import {FaIconLibrary} from '@fortawesome/angular-fontawesome';

@Component({
  selector: 'app-ticket-type-dropdown',
  templateUrl: './ticket-type-dropdown.component.html',
  styleUrls: ['./ticket-type-dropdown.component.css']
})
export class TicketTypeDropdownComponent implements OnInit {

  @Input() type: string;

  @Output() select = new EventEmitter();

  option: any;

  options = [
    {
      value: 'bug',
      caption: 'Bug',
      icon: 'icon-bug'
    },
    {
      value: 'feature-request',
      caption: 'Feature request',
      icon: 'icon-new'
    },
    {
      value: 'how-to',
      caption: 'How to',
      icon: 'icon-new'
    },
    {
      value: 'other',
      caption: 'Other',
      icon: 'icon-tools'
    }
  ];

  constructor(private library: FaIconLibrary) {
    this.library.addIcons(faChevronDown);
  }

  ngOnInit(): void {
  }

  ngOnChanges() {
    if (this.type) {
      this.option = this.options.find(option => option.value === this.type);
    }
  }

}
